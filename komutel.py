import requests
import csv
from bs4 import BeautifulSoup

base_url = "https://www.komutel.com"
page = requests.get(base_url+"/en/products")
raw = BeautifulSoup(page.content, 'html.parser')
products = raw.find_all('div', class_='desc_wrapper')
#Could be optimized to work as a cache file
titles_file = open("titles.csv", "w")
writer = csv.writer(titles_file)
for product in products:
    #could use only this to get the task done but its too simple
    product_name = product.find('h3', class_='themecolor').get_text()
    product_link = product.find('a').attrs['href']
    writer.writerow([product_name, base_url+product_link])
titles_file.close()